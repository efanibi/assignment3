#include <iostream>
#include <string>
#include "Page.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>
using namespace std;

int main()
{
    const int numpage=4;
    srand (time(NULL));

    Page* array[numpage];
    Page page1("http://page1");
    Page page2("http://page2");
    Page page3("http://page3");
    Page page4("http://page4");


    //Page 1
    page1.addLink(&page2);
    page1.addLink(&page3);

    //Page 2
    page2.addLink(&page3);

    //Page 3
    page3.addLink(&page1);

    //Page 4
    page4.addLink(&page1);
    page4.addLink(&page3);

    //Setup Array after Initializing
    array[0]=&page1;
    array[1]=&page2;
    array[2]=&page3;
    array[3]=&page4;

    Page* current=array[0];
    //Page Random
    for(int i=0;i<10000;i++)
    {
        int random=1+rand()%100;
        if(random<=15)
        {
            random=rand()%4;
            current=array[random];
        }
        else
        {
            current=current ->getRandomLink();
        }

        current ->visit();
    }

    for(int i=0;i<numpage;i++)
    {
        Page* printthis;
        printthis=array[i];
        printthis ->print();
    }






}
